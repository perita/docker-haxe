FROM opensuse/leap:15.4

COPY aseprite.spec /usr/src/packages/SPECS

RUN set -eux \
	&& zypper --non-interactive refresh \
	&& ASEPRITE_VERSION=$(grep Version /usr/src/packages/SPECS/aseprite.spec | cut -d: -f2 | tr -d ' ') \
	&& ASEPRITE_BUILD_REQUIRES=$(grep BuildRequires /usr/src/packages/SPECS/aseprite.spec | cut -d: -f2 | cut -d'>' -f1 | xargs zypper --non-interactive --table-style 10 search --provides --match-exact | grep -v 32bit | awk -F ':' '{ print $2;}' | grep -v '\sName\s') \
	&& cd /usr/src/packages/SOURCES \
	&& curl -LO $(grep Source /usr/src/packages/SPECS/aseprite.spec | cut -d: -f2- | tr -d ' ' | sed -e "s/%{version}/${ASEPRITE_VERSION}/g") \
	&& zypper --non-interactive install rpm-build $ASEPRITE_BUILD_REQUIRES \
	&& rpmbuild -bb /usr/src/packages/SPECS/aseprite.spec \
	&& mv /usr/src/packages/RPMS/$(uname -m)/*.rpm /tmp \
	&& rm -fr /usr/src/packages \
	&& mkdir -p /usr/src/packages/RPMS \
	&& mv /tmp/*.rpm /usr/src/packages/RPMS \
	&& (zypper --non-interactive remove --clean-deps rpmbuild $ASEPRITE_BUILD_REQUIRES || true) \
	&& zypper --non-interactive clean -a

COPY texturepacker /usr/local/bin

RUN set -eux \
	&& chmod +x /usr/local/bin/texturepacker \
	&& zypper --non-interactive addrepo "https://download.opensuse.org/repositories/home:/jfita/openSUSE_Leap_15.4/home:jfita.repo" \
	&& zypper --non-interactive --gpg-auto-import-keys refresh \
	&& zypper --non-interactive install --allow-unsigned-rpm /usr/src/packages/RPMS/*.rpm \
	&& zypper --non-interactive install \
		git-core \
		kratools \
		haxe \
		hashlink \
		fontgen \
		texturepacker \
		make \
		xmlstarlet \
		inkscape \
	&& rm -fr /usr/src \
	&& zypper --non-interactive clean -a
