#
# spec file for package aseprite
#
Name:           aseprite
Version:        1.2.40
Release:        0
Summary:        Command-line only version of Aseprite
License:        SUSE-EULA
Group:          Development/Tools/Building
URL:            https://www.aseprite.org/
Source:         https://github.com/aseprite/aseprite/releases/download/v%{version}/Aseprite-v%{version}-Source.zip
BuildRequires:  unzip
BuildRequires:  cmake >= 2.6
BuildRequires:  gcc-c++
BuildRequires:  glibc-devel
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(harfbuzz)
BuildRequires:  giflib-devel >= 5.1.0
BuildRequires:  libjpeg62-devel
BuildRequires:  googletest-devel
BuildRequires:  libpng16-compat-devel
BuildRequires:  tinyxml-devel
BuildRequires:  pkgconfig(pixman-1)
BuildRequires:  pkgconfig(xi)
BuildRequires:  pkgconfig(xcursor)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  cmark-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Aseprite is a program to create animated sprites for websites and games.  This
is a command-line only version inteded to be used in automated building, such
as inside CI/CD systems.

%prep
%setup -c -q

rm -r third_party/{cmark,curl,freetype2,harfbuzz,giflib,jpeg,libpng,libwebp,pixman-cmake,zlib,tinyxml}

%build
%cmake .. -DUSE_SHARED_GIFLIB=ON \
          -DUSE_SHARED_CMARK=ON \
          -DUSE_SHARED_JPEGLIB=ON \
          -DUSE_SHARED_LIBLOADPNG=ON \
          -DUSE_SHARED_LIBPNG=ON \
          -DUSE_SHARED_ZLIB=ON \
          -DUSE_SHARED_PIXMAN=ON \
          -DUSE_SHARED_FREETYPE=ON \
          -DUSE_SHARED_HARFBUZZ=ON \
          -DUSE_SHARED_GTEST=ON \
          -DFULLSCREEN_PLATFORM=ON \
          -DUSE_SHARED_TINYXML=ON \
          -DWITH_DESKTOP_INTEGRATION=OFF \
          -DENABLE_NEWS=OFF \
          -DENABLE_UPDATER=OFF \
          -DENABLE_WEBSOCKET=OFF \
          -DENABLE_DRM=OFF \
          -DENABLE_UI=OFF \
	  -DENABLE_WEBP=OFF \
          -DLAF_BACKEND=none

make aseprite %{?_smp_mflags}

%install
install -m755 -d %{buildroot}%{_bindir}
install -m755 build/bin/%{name} %{buildroot}%{_bindir}
strip -S %{buildroot}%{_bindir}/%{name}
install -m755 -d %{buildroot}%{_datadir}/%{name}/data
install -m644 data/gui.xml %{buildroot}%{_datadir}/%{name}/data

%files
%defattr(-,root,root)
%license EULA.txt
%doc README.md
%{_bindir}/%{name}
%{_datadir}/%{name}

%changelog
